function isOnScreen(element) {
            const curPos = element.offset();
            var curTop = curPos.top;
            var screenHeight = $(window).height();

            return (curTop > screenHeight) ? false : true;
        }

        function scroll() {
            const navbarHeight = 100;

            if (!isOnScreen($('#navbar'))) {
                if (window.scrollY < navbarHeight) {
                    var navbar = document.getElementById('navbar');

                    navbar.scrollIntoView({
                        block: 'end',
                        behavior: 'smooth'
                    });
                }
            }
        };
        setTimeout(scroll, 1000);