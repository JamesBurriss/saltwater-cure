$('.play-button').click(function() {
            const thisAlbumAudio = $(this).next().get(0);

            if (thisAlbumAudio.paused) {
                $('.play-button + audio').each(function() {
                    this.pause();
                });

                thisAlbumAudio.play();
            } else {
                thisAlbumAudio.pause();
            }
        });